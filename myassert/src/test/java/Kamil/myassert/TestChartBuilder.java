package Kamil.myassert;

import java.util.List;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.assertj.core.api.Assertions.*;

import builder.ChartBuilder;
import chart.ChartSerie;
import chart.ChartSetting;
import chart.ChartType;
import chart.Point;
import chart.SerieType;

public class TestChartBuilder {

private ChartBuilder chBuilder;
	
	@Before
	public void before(){
		chBuilder = new ChartBuilder();
	}
	
	@Test
	public void testSerieAdded() {
		
		// GIVEN
		String title = "test";
		ChartSerie chartSerie = new ChartSerie( );
		chartSerie.setLabel(title);
		
		// WHEN
		ChartSetting chartSetting = chBuilder.addSerie(chartSerie).build();
		
		// THEN
		assertThat(chartSetting.getSeries().get(0).getLabel()).isEqualTo(title);
	}
	
	@Test
	public void testWithSeries(){
		
		// GIVEN
		List<ChartSerie> list = new ArrayList<ChartSerie>();
			list.add(new ChartSerie());
			list.add(new ChartSerie());
			list.add(new ChartSerie());
		
		// WHEN
		ChartSetting chartSetting = chBuilder.withSeries(list).build();
		
		// THEN
		assertThat(chartSetting.getSeries()).hasSize(3);
	}
	
	@Test
	public void testWithTitle(){
		
		// GIVEN
		String title = "test";
		
		// WHEN
		ChartSetting chartSetting = chBuilder.withTitle(title).build();
		
		// THEN
		assertThat(chartSetting.getTitle()).isEqualTo(title);
	}
	
	@Test
	public void testWithLegend(){
		
		// GIVEN
		
		// WHEN
		ChartSetting chartSetting = chBuilder.withLegend().build();
		
		// THEN
		assertThat(chartSetting.isHaveLegend()).isTrue();
	}

	@Test
	public void testWithType(){
		
		// GIVEN
		ChartType chartType = ChartType.LinePoint;
		
		// WHEN
		ChartSetting chartSetting = chBuilder.withType(chartType).build();
		
		// THEN
		assertThat(chartSetting.getCharType()).isEqualTo(chartType);
	}
}