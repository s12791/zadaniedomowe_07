package Kamil.myassert;

import java.util.List;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.assertj.core.api.Assertions.*;

import builder.SerieBuilder;
import chart.ChartSerie;
import chart.Point;
import chart.SerieType;

public class TestSerieBuilder {
	
	private SerieBuilder sBuilder;
	
	@Before
	public void before(){
		sBuilder = new SerieBuilder();
	}

	@Test
	public void testPointsAdded() {
		
		//GIVEN
		Point point = new Point(1,2);
		
		//WHEN
		ChartSerie chartSerie = sBuilder.addPoints(point).build();
		
		//THEN
		assertThat(chartSerie.getPoints()).containsExactly(point);


	}
	
	@Test
	public void testLabelAdded(){
		
		//GIVEN
		String label = "test";
		
		//WHEN
		ChartSerie chartSerie = sBuilder.addLabel(label).build();
		
		//THEN
		assertThat(chartSerie.getLabel()).contains(label);
	}
	
	@Test
	public void testListPointAdded(){
		
		// GIVEN
		List<Point> point = new ArrayList<Point>();
			point.add( new Point( 1,1 ) );
			point.add( new Point( 1,2 ) );
			point.add( new Point( 1,3 ) );
			point.add( new Point( 1,4 ) );
		
		// WHEN
		ChartSerie chartSerie = sBuilder.withPoints(point).build();
			
		// THEN
		assertThat( chartSerie.getPoints()).hasSize( 4 );
	}

	@Test
	public void testTypeAddes(){
		
		// GIVEN
		SerieType type = SerieType.Line;
		
		// WHEN 
		ChartSerie chartSerie = sBuilder.setType(type).build();
		
		// THEN
		assertThat(chartSerie.getSerieType()).isEqualTo(type);
	}

}